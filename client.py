import socket

def main():
    host = 'localhost'
    port = 12345

    # Create a TCP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to the server
        s.connect((host, port))
        print(f"Connected to server: {host}:{port}")

        while True:
            # Receive the number from the server
            data = s.recv(1024)
            num = int(data.decode())
            print(f"Received from server: {num}")

            if num >= 100:
                break  # หยุดการทำงานของ Client เมื่อค่าตัวเลขเท่ากับ 100

            # Increment the number by 1
            num += 1

            # Send the updated number back to the server
            s.send(str(num).encode())

    except ConnectionRefusedError:
        print("Connection refused. Make sure the server is running.")
    finally:
        # Close the connection with the server
        s.close()
        print("Connection closed with server.")

if __name__ == "__main__":
    main()
