import socket

def main():
    host = 'localhost'
    port = 12345

    # Create a TCP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the address and port
    s.bind((host, port))

    # Listen for incoming connections
    s.listen()

    print(f"Server listening on {host}:{port}")

    while True:
        # Accept a connection from the client
        conn, addr = s.accept()
        print(f"Connected to client: {addr}")

        try:
            num = 1

            while num <= 100:
                # Send the number to the client
                conn.send(str(num).encode())

                # Receive the updated number from the client
                data = conn.recv(1024)
                num = int(data.decode())
                print(f"Received from client and incremented by 1: {num}")

                num += 1

        except (ValueError, ConnectionResetError):
            print("Invalid data received or client disconnected.")
        finally:
            # Close the connection with the client
            conn.close()
            print("Connection closed with client.")
            break  # หยุดการทำงานของ Server เมื่อค่าตัวเลขเท่ากับ 100

if __name__ == "__main__":
    main()
